//
//  AirportRow.swift
//  handoffexample
//
//  Copyright (c) 2015 iAchieved.it LLC. All rights reserved.
//

import Foundation
import WatchKit

class AirportRow : NSObject {
  
  @IBOutlet weak var airportName: WKInterfaceLabel!
  
}