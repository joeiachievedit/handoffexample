//
//  MapController.swift
//  handoffexample
//
//  Copyright (c) 2015 iAchieved.it LLC. All rights reserved.
//
import WatchKit
import Foundation


class WatchMapController: WKInterfaceController {
  
  @IBOutlet weak var mapView: WKInterfaceMap!
  var airport:Dictionary<String,String>?
  
  override func awakeWithContext(context: AnyObject?) {

    super.awakeWithContext(context)
    
    self.airport = (context as? Dictionary<String,String>)!
    
    let lat  = self.airport!["lat"]
    let lon  = self.airport!["lon"]
    let latitude  = (lat! as NSString).doubleValue
    let longitude = (lon! as NSString).doubleValue
    
    let location = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
    
    let span   = MKCoordinateSpanMake(0.1, 0.1)
    let region = MKCoordinateRegion(center: location, span: span)
    self.mapView.setRegion(region)
    

  }
  
  override func willActivate() {
    super.willActivate()
  }
  
  override func didDeactivate() {
    super.didDeactivate()
  }
  
}
