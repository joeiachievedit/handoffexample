//
//  InterfaceController.swift
//  handoffexample WatchKit Extension
//
//  Copyright (c) 2015 iAchieved.it LLC. All rights reserved.
//

import WatchKit
import Foundation


class InterfaceController: WKInterfaceController {
  
  @IBOutlet weak var airportsTable: WKInterfaceTable!
  
  override func awakeWithContext(context: AnyObject?) {
    super.awakeWithContext(context)
        
    self.airportsTable.setNumberOfRows(Airports.count, withRowType: "AirportRow")
    
    for var i = 0; i < Airports.count; i++ {
      
      var row = self.airportsTable.rowControllerAtIndex(i) as! AirportRow
      
      row.airportName.setText(Airports[i]["name"])
      
    }
    
  }
  
  override func willActivate() {
    super.willActivate()
    
  }
  
  override func contextForSegueWithIdentifier(segueIdentifier: String, inTable table: WKInterfaceTable, rowIndex: Int) -> AnyObject? {
    return Airports[rowIndex]
  }
  
  
  override func didDeactivate() {
    super.didDeactivate()
  }
  
}
