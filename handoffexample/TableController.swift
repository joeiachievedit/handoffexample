//
//  ViewController.swift
//  handoffexample
//
//  Copyright (c) 2015 iAchieved.it LLC. All rights reserved.
//

import UIKit

class TableController: UITableViewController {
  
  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view, typically from a nib.
  }
  
  override func viewWillAppear(animated: Bool) {
    self.title = "Airports"
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  // MARK:  Table functions
  override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return Airports.count
  }
  
  override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    
    let cellIdentifier = "AirportCell";
    let index = indexPath.row
    
    var cell = self.tableView.dequeueReusableCellWithIdentifier(cellIdentifier) as! UITableViewCell
    cell.textLabel?.text = Airports[index]["name"]
    
    return cell

  }
  
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    let index = self.tableView.indexPathForSelectedRow()
    let vc = segue.destinationViewController as! MapController
    vc.selectedIndex = index!.indexAtPosition(1)
  }




}

