//
//  MapController.swift
//  handoffexample
//
//  Copyright (c) 2015 iAchieved.it LLC. All rights reserved.
//

import UIKit
import MapKit

class MapController: UIViewController {
  
  @IBOutlet weak var mapView: MKMapView!
  
  var selectedIndex:Int = 0
  
  override func viewDidLoad() {
    super.viewDidLoad()
  }
  
  override func viewWillAppear(animated: Bool) {
    super.viewWillAppear(animated)
    
    let airport = Airports[self.selectedIndex]
    let name = airport["name"]
    let lat  = airport["lat"]!
    let lon  = airport["lon"]!
    
    let latitude  = (lat as NSString).doubleValue
    let longitude = (lon as NSString).doubleValue
    
    let location = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
    let span   = MKCoordinateSpanMake(0.1, 0.1)
    let region = MKCoordinateRegion(center: location, span: span)
    
    self.mapView.setRegion(region, animated: true)
    
    self.title = name

  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
  }
  
}

