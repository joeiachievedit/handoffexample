//
//  Airports.swift
//  handoffexample
//
//  Copyright (c) 2015 iAchieved.it LLC. All rights reserved.
//

import Foundation

let Airports = [
  ["name":"Carrasco",
    "iata":"MVD",
    "lat":"-34.838333",
    "lon":"-56.030833"
  ],
  ["name":"Dallas/Fort Worth",
   "iata":"DFW",
   "lat":"32.896944",
   "lon":"-97.038056"
  ],
  ["name":"Hartsfield–Jackson Atlanta",
    "iata":"ATL",
    "lat":"33.636667",
    "lon":"-84.428056"
  ],
  ["name":"Seattle–Tacoma",
    "iata":"SEA",
    "lat":"47.448889",
    "lon":"-122.309444"
  ],
]

